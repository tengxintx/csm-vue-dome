import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入全局样式
import '@/styles/global.css'
import '@/utils/element.js'
// 时间过滤
import dayjs from 'dayjs'

// 富文本
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// 导入富文本编辑器样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 全局注册富文本编辑器组件
Vue.use(VueQuillEditor)

Vue.filter('dataFormat', (v) => {
 return dayjs(v).format('YYYY-MM-DD hh:mm:ss')
 })
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
