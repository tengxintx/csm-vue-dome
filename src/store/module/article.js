import { articleData } from '@/api/article'
// 管理文章相关数据的仓库
export default {
    namespaced: true,
    state: {
        articleData: []
    },
    mutations: {
        setArticleData (state, articleData) {
            state.articleData = articleData
        }
    },
    actions: {
        async getArticleData (context) {
            const { data: { code, data, message } } = await articleData();
            if (code !== 0) return console.log(message);
            // console.log(data);
            context.commit('setArticleData', data)
        }
    }

}
