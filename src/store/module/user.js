import { infoGet } from '@/api/user.js'

export default {
    // ('user',[''])
    namespaced: true,
    state: {
        userInfo: {} // {}值
    },
    mutations: {
        setUserInfo (state, userInfo) {
            // 修改state的userInfo
            state.userInfo = userInfo
        }
    },
    actions: {
        async getUserInfo (context) {
            // 异步(ajax 或者 定时器)有结果的代码位置 commit mutations里面的方法
            const {
                data: { code, data, message }
            } = await infoGet()
            if (code !== 0) return console.log('fail')
            console.log(message)
            // 有结果了, commit传递给mutations
            context.commit('setUserInfo', data)
        }
    },
    getters: {
        // 显示名字
        uname (state) {
            return state.userInfo.username || state.userInfo.nickname
        },
        // 文字头像
        textAvatar (state) {
            // "abc".charAt(0) => a
            if (state.userInfo.username) {
                return state.userInfo.nickname.charAt(0).toUpperCase()
            } else {
                return '测'
            }
        }
    }
}
