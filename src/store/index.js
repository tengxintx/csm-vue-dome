import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/store/module/user'
import article from '@/store/module/article'

Vue.use(Vuex)

export default new Vuex.Store({

  modules: {
    article,
    user
  }
})
