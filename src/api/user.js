/* eslint-disable camelcase */
/* eslint-disable no-tabs */
// 封装用户的相关请求
import request from '@/utils/request.js'
// 按需导出  LoginPost返回的是promise对象  解构赋值
// 登录接口
export const LoginPost = ({ username, password }) => {
  return request.post('/api/login', {
    username,
    password
  })
}

// 注册接口
export const regPost = ({ username, password, repassword }) => {
  return request.post('/api/reg', {
    username,
    password,
    repassword
  })
}

// 获取信息
export const infoGet = () => {
  return request.get('/my/userinfo')
}

// 更新用户信息
export const updateUserInfo = ({ id, nickname, email }) => {
  return request.put('/my/userinfo', {
    id, nickname, email
  })
}

// 更新头像
export const reqUpdateAvatar = avatar => {
  return request({
    method: 'patch',
    url: '/my/update/avatar',
    data: {
      avatar
    }
  })
}

/*
old_pwd	是	string	原密码	非空字符串、长度 6-15
new_pwd	是	string	新密码	非空字符串、长度 6-15
re_pwd	是	string	确认新密码	非空字符串、长度 6-15
*/
// updatePwd
export const updatePwd = ({
  old_pwd,
  new_pwd,
  re_pwd
}) => request.patch('/my/updatepwd', {
  old_pwd,
  new_pwd,
  re_pwd
})
