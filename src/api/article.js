/* eslint-disable no-tabs */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-sequences */
/* eslint-disable camelcase */
// 封装用户的相关请求
import request from '@/utils/request.js'

// 获取文章列表数据
export const articleData = () => {
    return request.get('/my/cate/list')
}

// 添加文章分类
export const addCarList = ({ cate_name, cate_alias }) => {
    return request.post('/my/cate/add', {
        cate_name,
        cate_alias
    })
}

// 删除文章列表数据
export const removerCarList = id => {
    return request.delete('/my/cate/del', {
        params: { id }
    })
}

// 更新文章
export const UpdateArticleList = id => {
    return request.get('/my/cate/info', {
        params: { id }
    })
}

// 更新文章分类
export const isCategory = ({
    id,
    cate_name,
    cate_alias
}) => {
    return request.put('/my/cate/info', {
        id,
        cate_name,
        cate_alias
    })
}

/*

pagenum	是	number	页码值	整数，最小值为 1
pagesize	是	number	每页显示多少条数据	整数，最小值为 1
cate_id	否	string	文章分类的 id	整数，最小值为 1
state	否	string	文章的发布状态	可选值有：已发布、草稿 */

// 获取文章列表
export const articleList = ({
    pagenum,
    pagesize,
    cate_id,
    state
}) => {
    return request.get('/my/article/list', {
        params: {
            pagenum,
            pagesize,
            cate_id,
            state
        }
    })
}
// 删除文章列表
export const removerArtList = id => {
    return request.delete('/my/article/info', {
        params: { id }
    })
}
// 获取文章详细
export const articleDetailed = id => {
    return request.get('/my/article/info', {
        params: { id }
    })
}

/*
请求体（FormData 格式）：

参数名	必选	类型	说明	格式
title	是	string	文章标题	1-30 个字符
cate_id	是	int	所属分类 Id	整数，最小值为 1
content	是	string	文章内容	非空的字符串
state	是	string	文章的发布状态	可选值为：已发布、草稿
cover_img	是	blob二进制	文章封面	只能是图片文件
*/
// 发布新的文章
export const PublishedArticles = ({
    title,
    cate_id,
    content,
    state,
    cover_img
}) => {
    const fm = new FormData()
    fm.append('title', title)
    fm.append('cate_id', cate_id)
    fm.append('content', content)
    fm.append('state', state)
    fm.append('cover_img', cover_img)
    return request.post('/my/article/add', fm)
}
