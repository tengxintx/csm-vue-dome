// ElementUI按需导入的模块
import Vue from 'vue'
import 'element-ui/lib/theme-chalk/index.css'
import {
  Button,
  Form,
  FormItem,
  Input,
  Link,
  Message,
  Loading,
  Container,
  Header,
  Aside,
  Main,
  Menu,
  MenuItem,
  Submenu,
  MessageBox,
  Card,
  Breadcrumb,
  BreadcrumbItem,
  Row,
  Col,
  Table,
  TableColumn,
  Dialog,
  Select,
  Option,
  Pagination,
  Divider,
  Footer,
  MenuItemGroup

} from 'element-ui'
// 注册组件
Vue.use(Button)
  .use(Form)
  .use(FormItem)
  .use(Input)
  .use(Link)
  .use(Container)
  .use(Header)
  .use(Aside)
  .use(Main)
  .use(Menu)
  .use(MenuItem)
  .use(Submenu)
  .use(Card)
  .use(Breadcrumb)
  .use(BreadcrumbItem)
  .use(Row)
  .use(Col)
  .use(Table)
  .use(TableColumn)
  .use(Dialog)
  .use(Select)
  .use(Option)
  .use(Pagination)
  .use(Divider)
  .use(Footer)
  .use(MenuItemGroup)
// Message消息提示组件并不是复制组件结构使用的组件, 需要通过原型挂载的方式注册
Vue.prototype.$message = Message
Vue.prototype.$loading = Loading.service
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$msgbox = MessageBox
// .vue组件里面的this.$msgbox
// VueComponent

// 痛点: 不知道用啥 => 写业务 => 先分析 xxx => cv => 按需导入不用考虑了
