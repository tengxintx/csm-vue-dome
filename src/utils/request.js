/* eslint-disable indent */
// 请求axios模块
import axios from 'axios'
import router from '@/router/index'
import { Loading } from 'element-ui'

// 控制loading
let Controltheloading
const instance = axios.create({
    baseURL: 'http://www.liulongbin.top:3008'
})

// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // console.log('拦截前')
    // 拦截判定添加请求头
    if (config.url.startsWith('/my')) {
        config.headers.Authorization = window.localStorage.getItem('token')
    }
    Controltheloading = Loading.service({
        text: '拼命加载中！！！',
        spinner: 'el-icon-loading',
        background: 'rgba(0, 0, 0, 0.7)'
    })
    return config
}, function (error) {
    // 对请求错误做些什么
    return console.log(error)
})

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    Controltheloading.close()
    return response
}, function (error) {
    // 对响应错误做点什么
    // 移除token 并且转到登录页面
    window.localStorage.removeItem('token')
    // 先清除在跳转
    router.push('/login')

    Controltheloading.close()
    return error
})

export default instance
