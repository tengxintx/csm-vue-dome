// 导入路由
import Vue from 'vue'
import VueRouter from 'vue-router'
// import Login from '@/views/Login'
// import Reg from '@/views/reg'
// import Layout from '@/views/Layout'
// Layout 下的二级路由
// import Home from '@/views/Layout/Home'
// import ArtCategory from '@/views/Layout/Home/ArtCategory'
import ArtList from '@/views/Layout/Home/ArtList'
// import ChangeAvatar from '@/views/Layout/Home/ChangeAvatar'
// import UserInfo from '@/views/Layout/Home/UserInfo'
// import ResetPwd from '@/views/Layout/Home/ResetPwd'

const Login = () => import('@/views/Login')

const Reg = () => import('@/views/reg')

const Layout = () => import('@/views/Layout')

const Home = () => import('@/views/Layout/Home')

const ArtCategory = () => import('@/views/Layout/Home/ArtCategory')

// const ArtList = () => import('@views/Layout/Home/ArtList')

const ChangeAvatar = () => import('@/views/Layout/Home/ChangeAvatar')

const UserInfo = () => import('@/views/Layout/Home/UserInfo')

const ResetPwd = () => import('@/views/Layout/Home/ResetPwd')

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: Layout,

      // 二级路由
      children: [{
        path: '/',
        component: Home
      },
      {
        name: ArtList,
        path: '/artList',
        component: ArtList
      },
      {
        name: ChangeAvatar,
        path: '/changeAvatar',
        component: ChangeAvatar
      },
      {
        name: UserInfo,
        path: '/userInfo',
        component: UserInfo
      },
      {
        name: ResetPwd,
        path: '/resetPwd',
        component: ResetPwd
      },
      {
        name: ArtCategory,
        path: '/artCategory',
        component: ArtCategory
      }
      ]

    },
    {
      name: 'login',
      path: '/login',
      component: Login
    },
    {
      name: 'reg',
      path: '/reg',
      component: Reg
    }
  ]
})
// 路由守卫  防止token无，还可以访问界面
// to当前要显示的路由配置对象
// from要显示配置的路由显示界面
router.beforeEach((to, from, next) => {
  if (to.path === '/login' || to.path === '/reg') {
    next()
  } else {
    // 有token值放行
    if (window.localStorage.getItem('token')) {
      next()
    } else {
      next('./login')
    }
  }
})
export default router
