
// 覆盖默认的webpack配置
module.exports = {
    publicPath: './',
    devServer: {
        port: 4000,
        open: true
    },
    chainWebpack: (config) => {
        config.optimization.minimizer('terser').tap((args) => {
            args[0].terserOptions.compress.drop_console = true
            return args
        })
    }
}
